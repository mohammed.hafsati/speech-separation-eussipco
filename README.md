# Blind speech separation through direction of arrival estimation using deep neural networks with a flexibility on the number of speakers



## Audio examples

We have three mixtures; they may contain 2 or 3 sound sources. For each mixture, we provide the resulted separation using our solution. 

## Live Demo
The video contains a live demo where my colleague and I talk simultaneously and live record both our commands using the Reaspeaker (6 mics) microphone array. This microphone is connected to a raspberry pi that detects speech and sends it to a server that contains our speech separation solution. The algorithm is applied and yield the separation with a playback one after the other.  
